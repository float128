#define _GNU_SOURCE
#include <math.h>
#include <HsFFI.h>

HsDouble f128_get_d(const _Float128 *a) { return *a; }
HsInt    f128_get_i(const _Float128 *a) { return *a; }
void     f128_set_d(_Float128 *r, HsDouble b) { *r = b; }
void     f128_set_i(_Float128 *r, HsInt b) { *r = b; }

void f128_add(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = *a + *b; }
void f128_sub(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = *a - *b; }
void f128_mul(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = *a * *b; }
void f128_div(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = *a / *b; }
void f128_pow(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = powf128(*a, *b); }
void f128_atan2(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = atan2f128(*a, *b); }
void f128_min(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = fminf128(*a, *b); }
void f128_max(_Float128 *r, const _Float128 *a, const _Float128 *b) { *r = fmaxf128(*a, *b); }

int f128_lt(const _Float128 *a, const _Float128 *b) { return *a <  *b; }
int f128_le(const _Float128 *a, const _Float128 *b) { return *a <= *b; }
int f128_eq(const _Float128 *a, const _Float128 *b) { return *a == *b; }
int f128_ne(const _Float128 *a, const _Float128 *b) { return *a != *b; }
int f128_ge(const _Float128 *a, const _Float128 *b) { return *a >= *b; }
int f128_gt(const _Float128 *a, const _Float128 *b) { return *a >  *b; }

void f128_abs(_Float128 *r, const _Float128 *a) { *r = fabsf128(*a); }
void f128_sgn(_Float128 *r, const _Float128 *a) { *r = (*a > 0) - (0 > *a); }
void f128_neg(_Float128 *r, const _Float128 *a) { *r = - *a; }
void f128_recip(_Float128 *r, const _Float128 *a) { *r = 1.0L / *a; }
void f128_sqrt(_Float128 *r, const _Float128 *a) { *r = sqrtf128(*a); }
void f128_exp(_Float128 *r, const _Float128 *a) { *r = expf128(*a); }
void f128_log(_Float128 *r, const _Float128 *a) { *r = logf128(*a); }
void f128_sin(_Float128 *r, const _Float128 *a) { *r = sinf128(*a); }
void f128_cos(_Float128 *r, const _Float128 *a) { *r = cosf128(*a); }
void f128_tan(_Float128 *r, const _Float128 *a) { *r = tanf128(*a); }
void f128_sinh(_Float128 *r, const _Float128 *a) { *r = sinhf128(*a); }
void f128_cosh(_Float128 *r, const _Float128 *a) { *r = coshf128(*a); }
void f128_tanh(_Float128 *r, const _Float128 *a) { *r = tanhf128(*a); }
void f128_asin(_Float128 *r, const _Float128 *a) { *r = asinf128(*a); }
void f128_acos(_Float128 *r, const _Float128 *a) { *r = acosf128(*a); }
void f128_atan(_Float128 *r, const _Float128 *a) { *r = atanf128(*a); }
void f128_asinh(_Float128 *r, const _Float128 *a) { *r = asinhf128(*a); }
void f128_acosh(_Float128 *r, const _Float128 *a) { *r = acoshf128(*a); }
void f128_atanh(_Float128 *r, const _Float128 *a) { *r = atanhf128(*a); }
void f128_floor(_Float128 *r, const _Float128 *a) { *r = floorf128(*a); }
void f128_ceil(_Float128 *r, const _Float128 *a) { *r = ceilf128(*a); }
void f128_round(_Float128 *r, const _Float128 *a) { *r = roundf128(*a); }
void f128_trunc(_Float128 *r, const _Float128 *a) { *r = truncf128(*a); }

int f128_isnan(const _Float128 *a) { return fpclassify(*a) == FP_NAN; }
int f128_isinf(const _Float128 *a) { return fpclassify(*a) == FP_INFINITE; }
int f128_isdenorm(const _Float128 *a) { return fpclassify(*a) == FP_SUBNORMAL; }
int f128_isnegzero(const _Float128 *a) { return fpclassify(*a) == FP_ZERO && signbit(*a); }

void f128_ldexp(_Float128 *r, const _Float128 *a, int b) { *r = ldexpf128(*a, b); }
void f128_frexp(_Float128 *r, const _Float128 *a, int *b) { *r = frexpf128(*a, b); }

void f128_pi(_Float128 *r) { *r = M_PIf128; }
